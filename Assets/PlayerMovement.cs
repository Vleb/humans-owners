﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float sprint;
    [SerializeField] float jumpHeight;
    [SerializeField] CharacterController controller;
    float gravity = 25;
    Vector3 velocity;
    bool isJumping;
    bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = controller.isGrounded;
        if (isGrounded && velocity.y < 0)
            velocity.y = 0f;
        move();
        jump();
        velocity.y -= gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }

    private void move()
    {
        Vector3 move = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");
        controller.Move(move.normalized * speed * Time.deltaTime);
    }


    private void jump()
    {
        Debug.Log(isGrounded);
        if (Input.GetButtonDown("Jump") && isGrounded && !isJumping)
        {
            Debug.Log("COUCOU");
            velocity.y += Mathf.Sqrt(jumpHeight * gravity);
            isJumping = true;
        } else if (isGrounded && isJumping) {
            isJumping = false;
        }
    }
}
