﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardContent : MonoBehaviour
{
    public Text textEmptyPrefab;
    public Text textScorePrefab;
    public Text textDatePrefab;
    public Image separatorPrefab;

    void Start()
    {
        List<Score> scoreList = ScoreboardManager.getScoreList();
        if (scoreList.Count == 0)
        {
            Instantiate(textEmptyPrefab, transform);
        }
        else
        {
            scoreList.Sort((score0, score1) => score1.score.CompareTo(score0.score));
            RectTransform rt = textScorePrefab.GetComponent<RectTransform>();
            float py = 0.0f;
            float pyDelta = rt.rect.height;
            bool dispSep = false;
            foreach (Score score in scoreList)
            {
                if (dispSep)
                {
                    Image separator = Instantiate(separatorPrefab, transform);
                    rt = separator.GetComponent<RectTransform>();
                    rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y - py);
                }
                else
                {
                    dispSep = true;
                }
                Text textScore = Instantiate(textScorePrefab, transform);
                rt = textScore.GetComponent<RectTransform>();
                rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y - py);
                textScore.text = score.score.ToString();
                Text textDate = Instantiate(textDatePrefab, transform);
                rt = textDate.GetComponent<RectTransform>();
                rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y - py);
                textDate.text = score.time;
                py += pyDelta;
            }
            rt = GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, py);
        }
    }
}
