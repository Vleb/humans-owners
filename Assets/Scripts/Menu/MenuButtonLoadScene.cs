﻿using UnityEngine.SceneManagement;

public class MenuButtonLoadScene : MenuButton
{
    public string sceneName;

    protected override void ButtonAction()
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
