﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class mouseAI : MonoBehaviour
{
    public GameObject player;

    private NavMeshAgent nav;
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.angularSpeed = 90;
        if (!player)
        {
            player = GameObject.FindWithTag("Player");
        }
    }

    void Update()
    {
        float dist = Vector3.Distance(player.transform.position, transform.position);
        RaycastHit hit;

        if (dist < 10.0f && !Physics.Linecast(transform.position, player.transform.position, out hit))
        {
            if (nav.remainingDistance < 5.0f)
            {
                Vector3 angle = transform.position - player.transform.position;
                nav.destination = transform.position + angle.normalized * 10.0f;
            }
        }
        else if (dist > 20.0f)
        {
            nav.destination = transform.position;
        }
    }
}
