﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour
{
    public Gamemode gamemode;
    public Camera mainCamera;
    public Button resumeButton;
    public GameObject playerCamera;
    public EventSystem eventSystem;

    public float fadeSpeed = 0.05f;

    private DepthOfField dof;
    private RectMask2D canvasMask;

    private bool isAnimationPlaying = false;

    IEnumerator PauseAnimation()
    {
        isAnimationPlaying = true;
        bool resume = (int)gamemode.gameInProgress > 0;
        float almostHalfHeight = GetComponent<RectTransform>().sizeDelta.y / 2.0f;
        for (float f = 0.0f; f < 1.0f; f += fadeSpeed)
        {
            float invf = 1.0f - f;
            dof.focalLength.value = 1.0f + (resume ? invf : f) * 19.0f;
            canvasMask.padding = new Vector4(0.0f, (resume ? f : invf) * almostHalfHeight, 0.0f, (resume ? f : invf) * almostHalfHeight);
            yield return new WaitForSeconds(.01f);
        }
        dof.focalLength.value = 1.0f + (resume ? .0f : 19.0f);
        canvasMask.padding = new Vector4(0.0f, (resume ? almostHalfHeight : .0f), 0.0f, (resume ? almostHalfHeight : .0f));
        isAnimationPlaying = false;
    }

    void Start()
    {
        mainCamera.GetComponent<PostProcessVolume>().profile.TryGetSettings(out dof);
        canvasMask = GetComponent<RectMask2D>();
        resumeButton.onClick.AddListener(OpenClose);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && (int)gamemode.gameInProgress > -1)
        {
            OpenClose();
        }
    }

    void OpenClose()
    {
        if (!isAnimationPlaying)
        {
            gamemode.gameInProgress = (int)gamemode.gameInProgress > 0 ? Gamemode.GameState.Pause : Gamemode.GameState.Playing;
            StartCoroutine("PauseAnimation");
            if ((int)gamemode.gameInProgress > 0)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                playerCamera.SetActive(true);
                eventSystem.SetSelectedGameObject(null);
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                playerCamera.SetActive(false);
                resumeButton.enabled = true;
            }
        }
    }
}
