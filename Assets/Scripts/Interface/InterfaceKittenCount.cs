﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceKittenCount : MonoBehaviour
{
    public Gamemode gamemode;

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
        text.text = gamemode.kittenBrought.ToString() + (gamemode.kittenBrought == 69 ? " KITTENS SAVED, NICE!!!" : gamemode.kittenBrought > 1 ? " KITTENS SAVED" : " KITTEN SAVED");
    }
}
