﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceScoreCount : MonoBehaviour
{
    public Gamemode gamemode;

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
        text.text = "SCORE: " + gamemode.getScore().ToString();
    }
}
