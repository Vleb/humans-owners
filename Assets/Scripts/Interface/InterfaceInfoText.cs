﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InterfaceInfoText : MonoBehaviour
{
    public Gamemode gamemode;
    public Camera mainCamera;
    public Image fading;

    public float tutoTimeDelay = 5.0f;
    public float startTimeDelay = 0.5f;
    public float endTimeDelay = 3.0f;
    public float fadeSpeed = 0.05f;

    private List<string> tutoText = new List<string>
    {
        "It appears that you're starting your first game, yay!!!",
        "Here is all you need to know, so be attentive.",
        "To gain score, you must bring your kittens in your basket in the kitchen.\nYou can also catch mouses, that counts too.",
        "On the top left of your screen is a bar that shows your energy.\nCatching mouses will fill it up.",
        "If you end up with no energy left, the game is over.\nOf course you can pause the game at any moment using the escape key.",
        "On the top right of your screen is a minimap.\nIt will show you the mouses around you, but not the kittens.",
        "To move, use the arrow keys or WASD and sprint using Shift.\nTo attack or catch, use the left or right click of your mouse.",
        "Well that's it, good luck!"
    };

    private List<string> startText = new List<string>
    {
        "Ready?",
        "3",
        "2",
        "1",
        "GO!!!"
    };

    private bool textDisplayed = false;
    private int msgCount = 0;

    private Text thisText;
    private Color thisColor;
    private DepthOfField dof;

    private bool isStart;
    private bool doTuto;
    private float timeDelay;

    private List<string> textList = new List<string>();

    IEnumerator DisplayInfoText()
    {
        if (msgCount == 0)
        {
            textDisplayed = true;
            if (isStart)
            {
                // Black fade out
                for (float fa = 1.0f; fa > .0f; fa -= fadeSpeed)
                {
                    fading.color = new Color(fading.color.r, fading.color.g, fading.color.b, fa);
                    yield return new WaitForSeconds(.01f);
                }
                fading.color = new Color(fading.color.r, fading.color.g, fading.color.b, .0f);
            }

            // Dof fade in
            for (float f2 = 0.0f; f2 < 1.0f; f2 += fadeSpeed)
            {
                dof.focalLength.value = 1.0f + f2 * 19.0f;
                yield return new WaitForSeconds(.01f);
            }
            dof.focalLength.value = 20.0f;
        }

        thisText.text = textList[msgCount++];

        //Text fade in
        float f = 0.0f;
        for (; f < 1.0f; f += fadeSpeed)
        {
            thisColor.a = f;
            thisText.color = thisColor;
            yield return new WaitForSeconds(.01f);
        }
        thisColor.a = 1.0f;
        thisText.color = thisColor;

        yield return new WaitForSeconds(timeDelay);

        // Text fade out
        for (; f > .0f; f -= fadeSpeed)
        {
            thisColor.a = f;
            thisText.color = thisColor;
            yield return new WaitForSeconds(.01f);
        }
        thisColor.a = .0f;
        thisText.color = thisColor;

        if (msgCount == textList.Count)
        {
            msgCount = 0;
            if (isStart)
            {
                // Dof fade out
                for (float f2 = 1.0f; f2 > .0f; f2 -= fadeSpeed)
                {
                    dof.focalLength.value = 1.0f + f2 * 19.0f;
                    yield return new WaitForSeconds(.01f);
                }
                dof.focalLength.value = 1.0f;
                gamemode.gameInProgress = Gamemode.GameState.Playing;
                textDisplayed = false;
            }
            else
            {
                // Black fade in
                for (float fa = 0.0f; fa < 1.0f; fa += fadeSpeed)
                {
                    fading.color = new Color(fading.color.r, fading.color.g, fading.color.b, fa);
                    yield return new WaitForSeconds(.01f);
                }
                fading.color = new Color(fading.color.r, fading.color.g, fading.color.b, 1.0f);

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene("ScoreboardMenu", LoadSceneMode.Single);
            }
        }
        else
        {
            if (doTuto && msgCount == tutoText.Count)
            {
                timeDelay = startTimeDelay;
                doTuto = false;
            }
            StartCoroutine("DisplayInfoText");
        }
    }

    void Start()
    {
        thisText = GetComponent<Text>();
        thisColor = thisText.color;
        mainCamera.GetComponent<PostProcessVolume>().profile.TryGetSettings(out dof);

        isStart = true;
        doTuto = !PlayerPrefs.HasKey("firstTime");
        if (doTuto)
        {
            PlayerPrefs.SetInt("firstTime", 1);
            tutoText.ForEach(p => textList.Add(p));
            timeDelay = tutoTimeDelay;
        } 
        else
        {
            timeDelay = startTimeDelay;
        }
        startText.ForEach(p => textList.Add(p));

        gamemode.gameInProgress = Gamemode.GameState.PauseNoMenu;
        StartCoroutine("DisplayInfoText");
    }

    void Update()
    {
        if (!textDisplayed && gamemode.gameInProgress == Gamemode.GameState.GameOver)
        {
            isStart = false;
            timeDelay = endTimeDelay;
            textList = new List<string>
            {
                "Game Over",
                "You scored " + gamemode.getScore().ToString() + " points!"
            };
            StartCoroutine("DisplayInfoText");
        }
    }
}
