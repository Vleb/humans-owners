﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceMinimap : MonoBehaviour
{
	public GameObject player;
	public Image indicatorPrefab;

	public Image minimapImage;
	public Text minimapText;

	public bool displayCoordText = false;

	private RectTransform minimapRect;
	private List<Image> indicatorList;

	void Start()
    {
		minimapRect = minimapImage.GetComponent<RectTransform>();
		indicatorList = new List<Image>();
	}

	void Update()
    {
		float playerRotation = Mathf.Deg2Rad * (player.transform.eulerAngles.y + 180.0f);
		Vector3 playerPos = player.transform.position;

		GameObject[] targetList = GameObject.FindGameObjectsWithTag("Target");
		int indicatorCount = 0;
		foreach (GameObject target in targetList)
		{
			Vector3 targetPos = target.transform.position;
			Vector3 entityDirection = Vector3.Normalize(targetPos - playerPos);
			Vector3 targetPosCorrected = playerPos + entityDirection * Mathf.Min(Vector3.Distance(targetPos, playerPos), 22.5f);

			float playerDistance = Vector3.Distance(playerPos, targetPos);
			if (playerDistance > 0)
			{
				// Create a new indicator instance if no one is available otherwise take an existing one
                Image minimapIndicator;
				if (indicatorCount >= indicatorList.Count)
                {
					minimapIndicator = Instantiate(indicatorPrefab, minimapImage.transform);
					indicatorList.Add(minimapIndicator);
                }
				else
                {
					minimapIndicator = indicatorList[indicatorCount];
                }
				indicatorCount++;

				// Update the indicator's color and position
				Vector3 characterInterfaceDiff = new Vector3(
					(playerPos.x - targetPosCorrected.x) * 6.0f,
					1.0f - Mathf.Clamp(Mathf.Abs(playerPos.y - targetPosCorrected.y) / 3.0f, 0.0f, 0.9f),
					(playerPos.z - targetPosCorrected.z) * 6.0f
				);
				minimapIndicator.color = new Color(1.0f, 1.0f, 1.0f, characterInterfaceDiff.y);
				float cosPlayerRotation = Mathf.Cos(playerRotation);
				float sinPlayerRotation = Mathf.Sin(playerRotation);
				minimapIndicator.GetComponent<RectTransform>().anchoredPosition = new Vector2(
					characterInterfaceDiff.x * cosPlayerRotation - characterInterfaceDiff.z * sinPlayerRotation,
					characterInterfaceDiff.z * cosPlayerRotation + characterInterfaceDiff.x * sinPlayerRotation
				);
			}
		}

		// Remove excessive indicator instances
		int garbageCount = indicatorCount - targetList.Length;
		for (int i = 0; i < garbageCount; i++)
        {
			indicatorList.RemoveAt(indicatorList.Count - 1);
        }

		// Update minimap text
		if (displayCoordText)
        {
			minimapText.text = targetList.Length.ToString();// playerPos.x.ToString() + " / " + playerPos.z.ToString();
		}
	}
}
