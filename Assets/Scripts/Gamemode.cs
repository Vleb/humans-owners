﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gamemode : MonoBehaviour
{
    public float startTime = 120.0f;
    public float maximumTime = 300.0f;
    public float killTimeGain = 60.0f;
    public int killScoreGain = 1000;
    public int disposalScoreGain = 10000;

    private float _currentTime;
    public float currentTime
    {
        get { return _currentTime; }
        set
        {
            _currentTime = value;
            _currentTime = Mathf.Clamp(_currentTime, 0.0f, maximumTime);
        }
    }

    public enum GameState
    {
        GameOver = -2,
        PauseNoMenu = -1,
        Pause = 0,
        Playing = 1
    };

    [HideInInspector] public float timeMultiplier = 1.0f;
    [HideInInspector] public bool isCarryingKitten = false;
    [HideInInspector] public GameState gameInProgress = GameState.Playing;
    [HideInInspector] public int mouseKilled = 0;
    [HideInInspector] public int kittenBrought = 0;
    private float timePoints = 0.0f;

    public GameObject kittenPrefab;
    public Transform[] kittensSpawns;
    [HideInInspector] public List<GameObject> kittens;
    public int kittensMax = 5;

    public GameObject mousePrefab;
    public Transform[] micesSpawns;
    [HideInInspector] public List<GameObject> mices;
    public int micesMax = 5;

    public int maxIter = 10;

    private System.Random rand;

    public GameObject player;

    public float spawnDist;

    void Start()
    {
        currentTime = startTime;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        rand = new System.Random();
        kittens = new List<GameObject>();
        mices = new List<GameObject>();
        //checkSpawnables();
    }

    void Update()
    {
        if ((int)gameInProgress > 0)
        {
            currentTime -= Time.deltaTime * timeMultiplier * (isCarryingKitten ? 2.0f : 1.0f);
            timePoints += Time.deltaTime;
            if (currentTime <= 0.0)
            {
                gameInProgress = GameState.GameOver;
            }
        }
        //checkSpawnables();
    }

    public int getScore()
    {
        return kittenBrought * disposalScoreGain + mouseKilled * killScoreGain + (int)(timePoints * timePoints);
    }

    void OnDestroy()
    {
        ScoreboardManager.registerScore(getScore());
    }

    void checkSpawnables()
    {
        int i = kittensMax - kittens.Count;
        while (i >= 0 && kittens.Count < kittensMax && kittens.Count < kittensSpawns.Length)
        {
            SearchSpawn(kittenPrefab, kittens, kittensSpawns);
            i--;
        }
        i = micesMax - mices.Count;
        while (i >= 0 && mices.Count < micesMax && mices.Count < micesSpawns.Length)
        {
            SearchSpawn(mousePrefab, mices, micesSpawns);
            i--;
        }
    }

    List<GameObject> SearchSpawn(GameObject prefab, List<GameObject> alives, Transform[] spawns)
    {
        int idx = rand.Next(spawns.Length);
        bool blocked = false;

        if (alives.Count == 0)
        {
            return SpawnEntity(prefab, alives, spawns[idx], tag);
        }
        for (int iterr = 0; iterr < maxIter; iterr++)
        {
            blocked = false;

            float playerDist = Vector3.Distance(spawns[idx].position, player.transform.position);
            if (playerDist > spawnDist)
            {
                foreach (GameObject alive in alives)
                {
                    float objDist = Vector3.Distance(spawns[idx].position, alive.transform.position);
                    if (objDist < 3.0f)
                    {
                        blocked = true;
                    }
                }
                if (!blocked)
                {
                    return SpawnEntity(prefab, alives, spawns[idx], tag);
                }
            }
            idx = rand.Next(spawns.Length);
        }
        return alives;
    }

    List<GameObject> SpawnEntity(GameObject prefab, List<GameObject> alives, Transform spawn, string tag)
    {
        GameObject newEntity = Instantiate(prefab, spawn.position, spawn.rotation) as GameObject;
        if (prefab.tag == "Kitten")
        {
            int nbSkin = Random.Range(1, 13);
            int nbEyes = Random.Range(1, 13);
            string skinName = "Cat_mat_" + nbSkin;
            string eyesName = "Cat_mat_" + nbEyes;

            GameObject skin = LibGameObject.FindChildByName(newEntity, "CatGirl2:cat_house1");
            GameObject leftEye = LibGameObject.FindChildByName(newEntity, "CatGirl2:eye_L009");
            GameObject rightEye = LibGameObject.FindChildByName(newEntity, "CatGirl2:eye_R009");

            Material eyesMaterial = Resources.Load(eyesName, typeof(Material)) as Material;

            var materialsSkin = skin.GetComponent<SkinnedMeshRenderer>().materials;
            materialsSkin[0] = Resources.Load(skinName, typeof(Material)) as Material;
            skin.GetComponent<SkinnedMeshRenderer>().materials = materialsSkin;

            var materialsLeftEye = leftEye.GetComponent<SkinnedMeshRenderer>().materials;
            materialsLeftEye[0] = eyesMaterial;
            leftEye.GetComponent<SkinnedMeshRenderer>().materials = materialsLeftEye;

            var materialsRightEye = rightEye.GetComponent<SkinnedMeshRenderer>().materials;
            materialsRightEye[0] = eyesMaterial;
            rightEye.GetComponent<SkinnedMeshRenderer>().materials = materialsRightEye;
        }
        alives.Add(newEntity);
        return alives;
    }

    public void killMouse(GameObject mouse)
    {
        mouseKilled++;
        currentTime += killTimeGain;
        mouse.tag = "Untagged";
        mices.Remove(mouse);
        Destroy(mouse);
    }

    public void retrieveKitten(GameObject kitten)
    {
        kittenBrought++;
        kittens.Remove(kitten);
        Destroy(kitten);
    }
}
