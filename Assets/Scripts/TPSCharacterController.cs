﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSCharacterController : MonoBehaviour
{
    public CharacterController characterController;
    public Transform cameraTransform;
    public Gamemode gamemode;
    public Transform catTransform;

    public float speed = 3.0f;
    public float sprintMult = 2.0f;
    public float jumpHeight = 3.0f;

    private Animator m_Animator;

    private float gravity;
    private float turnVelocity;
    private float speedVelocity;
    private float sprintSpeed;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_Animator = GetComponent<Animator>();
    }

    // void OnControllerColliderHit(ControllerColliderHit hit) {
    //   if (Input.GetKey(KeyCode.Space) && !characterController.isGrounded) {
    //     gravity += 10.0f * Time.deltaTime;
    //     catTransform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
    //   }
    // }

    void Update()
    {
        if ((int)gamemode.gameInProgress > 0)
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(horizontal, 0.0f, vertical).normalized;

            float sp;
            if (Input.GetKey(KeyCode.LeftShift) && (horizontal != 0.0f || vertical != 0.0f))
            {
                sp = speed * sprintMult;
                gamemode.timeMultiplier = 3.0f;
            }
            else
            {
                sp = speed;
                gamemode.timeMultiplier = 1.0f;
            }
            sprintSpeed = Mathf.SmoothDampAngle(sprintSpeed, sp, ref speedVelocity, 0.5f);

            Vector3 move = new Vector3();
            if (direction.magnitude >= 0.1f)
            {
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cameraTransform.eulerAngles.y;
                float angleSmooth = Mathf.SmoothDampAngle(transform.eulerAngles.y, angle, ref turnVelocity, 0.1f * Mathf.Pow(sprintSpeed / speed, 0.5f));
                transform.rotation = Quaternion.Euler(0.0f, angleSmooth, 0.0f);

                Vector3 actualDirection = Quaternion.Euler(0.0f, angleSmooth, 0.0f) * Vector3.forward;
                move = actualDirection.normalized;
            }

            move *= sprintSpeed * Time.deltaTime;

            if (characterController.isGrounded)
            {
                gravity = 0.0f;
                if (Input.GetKeyDown(KeyCode.Space))
                    gravity = jumpHeight;
            }

            m_Animator.speed = sprintSpeed / speed * 0.75f;

            gravity -= 9.81f * Time.deltaTime;
            move.y = gravity * Time.deltaTime;
            characterController.Move(move);

            bool hasmove = false;
            if (vertical != 0.0f || horizontal != 0.0f)
            {
                m_Animator.ResetTrigger("Idle");

                //Send the message to the Animator to activate the trigger parameter named "Run"
                m_Animator.SetTrigger("Run");
                hasmove = true;
            }
            if (Input.GetMouseButtonDown(1))
            {
                //Send the message to the Animator to activate the trigger parameter named "TakeKitten"
                m_Animator.SetTrigger("TakeKitten");
                hasmove = true;
            }
            if (Input.GetMouseButtonDown(0))
            {
                //Send the message to the Animator to activate the trigger parameter named "attack"
                m_Animator.SetTrigger("Attack");
                hasmove = true;
            }
            if (hasmove == false)
            {
                m_Animator.ResetTrigger("Run");
                m_Animator.SetTrigger("Idle");
            }
        }
        else
        {
            m_Animator.ResetTrigger("Run");
            m_Animator.SetTrigger("Idle");
        }
    }
}
