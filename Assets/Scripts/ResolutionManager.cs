﻿using UnityEngine;

public class ResolutionManager : MonoBehaviour
{
    void Start()
    {
        if (PlayerPrefs.HasKey("settingsResolution"))
        {
            Resolution selectedResolution = Screen.resolutions[PlayerPrefs.GetInt("settingsResolution")];
            Screen.SetResolution(selectedResolution.width, selectedResolution.height, true, selectedResolution.refreshRate);
        }
    }
}
