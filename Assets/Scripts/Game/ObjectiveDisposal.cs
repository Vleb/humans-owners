﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ObjectiveDisposal : MonoBehaviour
{
    public ParticleSystem particleScore;
    public Gamemode gamemode;
    public AudioSource audioObjective;

    public string kittenTag = "Kitten";
    public float scoreDistance = 0.75f;
    public float scoreAnimSpeed = 0.01f;

    private IEnumerator disposeKitten(GameObject kitten)
    {
        ParticleSystem.MainModule pMain = particleScore.main;
        ParticleSystem.EmissionModule pEmissionModule = particleScore.emission;
        Color pColor = pMain.startColor.color;
        float pSpeed = 0.1f;
        float pEmissionRate = 5.0f;

        audioObjective.Play();

        float f = 1.0f;
        for (; f >= -0.1f; f -= scoreAnimSpeed)
        {
            float actuallyF = Mathf.Max(0.0f, f);
            pColor.r = actuallyF;
            pColor.b = actuallyF;
            pMain.startColor = pColor;
            pMain.startSpeed = pSpeed * (3.0f - actuallyF * 2.0f);
            pEmissionModule.rateOverTime = pEmissionRate * (4.0f - actuallyF * 3.0f);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(1.0f);
        gamemode.retrieveKitten(kitten);
        for (; f <= 1.1f; f += scoreAnimSpeed)
        {
            float actuallyF = Mathf.Min(1.0f, f);
            pColor.r = actuallyF;
            pColor.b = actuallyF;
            pMain.startColor = pColor;
            pMain.startSpeed = pSpeed * (3.0f - actuallyF * 2.0f);
            pEmissionModule.rateOverTime = pEmissionRate * (4.0f - actuallyF * 3.0f);
            yield return new WaitForSeconds(.01f);
        }
    }

    void Update()
    {
        GameObject[] kittenList = GameObject.FindGameObjectsWithTag(kittenTag);
        Vector3 objectivePos = transform.position;
        foreach (GameObject kitten in kittenList)
        {
            // Check if kitten is currently being carried and has not already be taken into account
            if (kitten.GetComponent<SphereCollider>().enabled && !kitten.GetComponent<Animator>().GetBool("atObjective"))
            {
                Vector3 kittenPos = kitten.transform.position;
                float kittenDistance = Vector3.Distance(objectivePos, kittenPos);
                if (kittenDistance < scoreDistance)
                {
                    kitten.GetComponent<Animator>().SetBool("atObjective", true);
                    StartCoroutine(disposeKitten(kitten));
                }
            }
        }
    }
}
