﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KittenSpawner : MonoBehaviour
{
    public string kittenSpawnerTag = "KittenSpawner";
    public string mouseSpawnerTag = "MouseSpawner";

    public int maxKitten = 3;
    public int maxMices = 3;

    public GameObject player;

    public GameObject kittenPrefab;
    public GameObject mousePrefab;

    private GameObject[] kittenSpawnerList;
    private GameObject[] mouseSpawnerList;

    private float mouseSpawnMultiplier = 1.0f;

    void SpawnKittenEntity(Transform spawn)
    {
        GameObject newEntity = Instantiate(kittenPrefab, spawn.position, spawn.rotation) as GameObject;

        int nbSkin = Random.Range(1, 13);
        int nbEyes = Random.Range(1, 13);
        string skinName = "Cat_mat_" + nbSkin;
        string eyesName = "Cat_mat_" + nbEyes;

        GameObject skin = LibGameObject.FindChildByName(newEntity, "CatGirl2:cat_house1");
        GameObject leftEye = LibGameObject.FindChildByName(newEntity, "CatGirl2:eye_L009");
        GameObject rightEye = LibGameObject.FindChildByName(newEntity, "CatGirl2:eye_R009");

        Material eyesMaterial = Resources.Load(eyesName, typeof(Material)) as Material;

        var materialsSkin = skin.GetComponent<SkinnedMeshRenderer>().materials;
        materialsSkin[0] = Resources.Load(skinName, typeof(Material)) as Material;
        skin.GetComponent<SkinnedMeshRenderer>().materials = materialsSkin;

        var materialsLeftEye = leftEye.GetComponent<SkinnedMeshRenderer>().materials;
        materialsLeftEye[0] = eyesMaterial;
        leftEye.GetComponent<SkinnedMeshRenderer>().materials = materialsLeftEye;

        var materialsRightEye = rightEye.GetComponent<SkinnedMeshRenderer>().materials;
        materialsRightEye[0] = eyesMaterial;
        rightEye.GetComponent<SkinnedMeshRenderer>().materials = materialsRightEye;
    }

    IEnumerator SpawnKitten()
    {
        List<GameObject> freeSpawner = new List<GameObject>();
        GameObject[] kittenList = GameObject.FindGameObjectsWithTag("Kitten");
        if (kittenList.Length < maxKitten)
        {
            foreach (GameObject spawner in kittenSpawnerList)
            {
                if (Vector3.Distance(player.transform.position, spawner.transform.position) > 5.0f)
                {
                    bool spawnerIsFree = true;
                    foreach (GameObject kitten in kittenList)
                    {
                        if (Vector3.Distance(kitten.transform.position, spawner.transform.position) < 1.0f)
                        {
                            spawnerIsFree = false;
                            break;
                        }
                    }
                    if (spawnerIsFree)
                    {
                        freeSpawner.Add(spawner);
                    }
                }
            }
            if (freeSpawner.Count > 0)
            {
                GameObject randomSpawner = freeSpawner[Random.Range(0, freeSpawner.Count - 1)];
                SpawnKittenEntity(randomSpawner.transform);
            }
        }
        yield return new WaitForSeconds(10.0f);
        StartCoroutine("SpawnKitten");
    }

    IEnumerator SpawnMouse()
    {
        List<GameObject> freeSpawner = new List<GameObject>();
        GameObject[] mouseList = GameObject.FindGameObjectsWithTag("Target");
        if (mouseList.Length < maxMices)
        {
            foreach (GameObject spawner in mouseSpawnerList)
            {
                if (Vector3.Distance(player.transform.position, spawner.transform.position) > 5.0f)
                {
                    bool spawnerIsFree = true;
                    foreach (GameObject mouse in mouseList)
                    {
                        if (Vector3.Distance(mouse.transform.position, spawner.transform.position) < 1.0f)
                        {
                            spawnerIsFree = false;
                            break;
                        }
                    }
                    if (spawnerIsFree)
                    {
                        freeSpawner.Add(spawner);
                    }
                }
            }
            if (freeSpawner.Count > 0)
            {
                GameObject randomSpawner = freeSpawner[Random.Range(0, freeSpawner.Count - 1)];
                Instantiate(mousePrefab, randomSpawner.transform.position, randomSpawner.transform.rotation);
                mouseSpawnMultiplier *= 1.1f;
            }
        }
        yield return new WaitForSeconds(10.0f * mouseSpawnMultiplier);
        StartCoroutine("SpawnMouse");
    }

    void Start()
    {
        kittenSpawnerList = GameObject.FindGameObjectsWithTag(kittenSpawnerTag);
        mouseSpawnerList = GameObject.FindGameObjectsWithTag(mouseSpawnerTag);
        StartCoroutine("SpawnKitten");
        StartCoroutine("SpawnMouse");
    }
}
