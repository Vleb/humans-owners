﻿using UnityEngine;

public class CarryKitten : MonoBehaviour
{
    public GameObject playerJawBone;
    public Gamemode gamemode;
    public AudioSource audioTake;
    public AudioSource audioDrop;

    public string kittenTag = "Kitten";
    public float grabDistance = 0.5f;

    [HideInInspector] public GameObject carriedKitten = null;

    void Update()
    {
        if ((int)gamemode.gameInProgress > 0 && Input.GetMouseButtonDown(1))
        {
            // Grab kitten
            if (carriedKitten == null)
            {
                audioTake.Play();
                GameObject[] kittenList = GameObject.FindGameObjectsWithTag(kittenTag);
                Vector3 playerPos = transform.position;
                foreach (GameObject kitten in kittenList)
                {
                    Vector3 kittenPos = kitten.transform.position;
                    float kittenDistance = Vector3.Distance(playerPos, kittenPos);
                    if (kittenDistance < grabDistance)
                    {
                        gamemode.isCarryingKitten = true;
                        carriedKitten = kitten;
                        carriedKitten.GetComponent<SphereCollider>().enabled = false;
                        carriedKitten.GetComponent<Animator>().SetTrigger("taken");
                        break;
                    }
                }
            }

            // Drop kitten
            else
            {
                audioDrop.Play();
                gamemode.isCarryingKitten = false;
                Vector3 newKittenPos = playerJawBone.transform.position;
                newKittenPos.y += 0.05f;
                carriedKitten.transform.position = newKittenPos;
                carriedKitten.transform.rotation = Quaternion.identity;
                carriedKitten.GetComponent<Rigidbody>().velocity = Vector3.zero;
                carriedKitten.GetComponent<SphereCollider>().enabled = true;
                carriedKitten.GetComponent<Animator>().SetTrigger("drop");
                carriedKitten = null;
            }
        }

        // Update carried kitten position & rotation
        if (carriedKitten != null)
        {
            Vector3 newKittenPos = playerJawBone.transform.position;
            newKittenPos.y -= 0.1f;
            Vector3 newKittenRotEuler = playerJawBone.transform.rotation.eulerAngles;
            newKittenRotEuler.z += 180.0f;
            Quaternion newKittenRot = Quaternion.Euler(newKittenRotEuler);
            carriedKitten.transform.position = newKittenPos;
            carriedKitten.transform.rotation = newKittenRot;
        }
    }
}
