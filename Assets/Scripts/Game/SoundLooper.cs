﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLooper : MonoBehaviour
{
    public int soundInterMin = 1;
    public int soundInterMax = 10;

    private AudioSource[] soundList;

    IEnumerator soundLoop()
    {
        yield return new WaitForSeconds(Random.Range(soundInterMin, soundInterMax));
        AudioSource sound = soundList[Random.Range(0, soundList.Length - 1)];
        sound.Play();
        while (sound.isPlaying)
        {
            yield return new WaitForSeconds(.1f);
        }
        StartCoroutine("soundLoop");
    }

    void Start()
    {
        soundList = GetComponentsInChildren<AudioSource>();
        StartCoroutine("soundLoop");
    }
}
